﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EASendMail;
using MailSmsApi.Models;
using System.Web.Mail;
using System.Net.Mail;
using MailSmsApi.Class;
using System.Net.Mime;

namespace MailSmsApi.Controllers
{

	public class ValuesController : ApiController
	{
		DB_Mail_ApiEntities db = new DB_Mail_ApiEntities();


		// GET api/values
		public IEnumerable<string> Get()
		{

			return new string[] { "value1", "value2" };
		}

		// GET api/values/5
		public string Get(int id)
		{
			return "value";
		}

		[Route("api/SMS/sender2")]
		public bool Getsender2(string Texto)
		{

			string textoRectificado = "";

			foreach (var item in Texto)
			{
				char Current = item;
				if (Current == 'á')
				{
					Current = 'a';
				}
				if (Current == 'é')
				{
					Current = 'e';
				}
				if (Current == 'í')
				{
					Current = 'i';
				}
				if (Current == 'ó')
				{
					Current = 'o';
				}
				if (Current == 'ú')
				{
					Current = 'u';
				}
				textoRectificado += Current;
			}


			try
			{
				EASendMail.SmtpMail oMail = new EASendMail.SmtpMail("TryIt");
				oMail.From = "desarrollo@calisis.com";
				oMail.To = "transferoee@gmail.com";
				oMail.Subject = "#SMS# Sampras OEE";
				oMail.TextBody = textoRectificado;
				//SmtpServer oServer = new SmtpServer("smtp.gmail.com");
				Mail_Configuration MailConfigurationObject = new Mail_Configuration();
				using (System.IO.StreamReader file = System.IO.File.OpenText(System.Web.HttpContext.Current.Server.MapPath("~/mailConfig.json")))
				{
					MailConfigurationObject = new Mail_Configuration(file);
				}
				SmtpServer oServer = new SmtpServer(MailConfigurationObject.SmtpServer);
				oServer.User = MailConfigurationObject.mailUser;
				oServer.Password = MailConfigurationObject.mailPass;
				oServer.Port = MailConfigurationObject.port;
				oServer.ConnectType = SmtpConnectType.ConnectSSLAuto;
				EASendMail.SmtpClient oSmtp = new EASendMail.SmtpClient();
				oSmtp.SendMail(oServer, oMail);
				return true;
			}
			catch (Exception e)
			{
				return false;
			}
		}

		[Route("api/SMS/sender2")]
		public bool Getsender2(string Texto, string Languaje)
		{

			string textoRectificado = "";

			foreach (var item in Texto)
			{
				char Current = item;
				if (Current == 'á')
				{
					Current = 'a';
				}
				if (Current == 'é')
				{
					Current = 'e';
				}
				if (Current == 'í')
				{
					Current = 'i';
				}
				if (Current == 'ó')
				{
					Current = 'o';
				}
				if (Current == 'ú')
				{
					Current = 'u';
				}
				textoRectificado += Current;
			}


			try
			{
				EASendMail.SmtpMail oMail = new EASendMail.SmtpMail("TryIt");
				oMail.From = "desarrollo@calisis.com";
				oMail.To = "transferoee@gmail.com";
				oMail.Subject = "#SMS# Sampras OEE";
				oMail.TextBody = textoRectificado + "|" + Languaje;
				Mail_Configuration MailConfigurationObject = new Mail_Configuration();
				using (System.IO.StreamReader file = System.IO.File.OpenText(System.Web.HttpContext.Current.Server.MapPath("~/mailConfig.json")))
				{
					MailConfigurationObject = new Mail_Configuration(file);
				}
				SmtpServer oServer = new SmtpServer(MailConfigurationObject.SmtpServer);
				oServer.User = MailConfigurationObject.mailUser;
				oServer.Password = MailConfigurationObject.mailPass;
				oServer.Port = MailConfigurationObject.port;
				oServer.ConnectType = SmtpConnectType.ConnectSSLAuto;
				EASendMail.SmtpClient oSmtp = new EASendMail.SmtpClient();
				oSmtp.SendMail(oServer, oMail);
				return true;
			}
			catch (Exception e)
			{
				return false;
			}
		}

		[Route("api/SMS/sender")]
		public bool Getsender(string Numbers, string Texto)
		{
			if (Numbers == null)
			{
				Numbers = "";
			}

			string textoRectificado = "";

			foreach (var item in Texto)
			{
				char Current = item;
				if (Current == 'á')
				{
					Current = 'a';
				}
				if (Current == 'é')
				{
					Current = 'e';
				}
				if (Current == 'í')
				{
					Current = 'i';
				}
				if (Current == 'ó')
				{
					Current = 'o';
				}
				if (Current == 'ú')
				{
					Current = 'u';
				}
				textoRectificado += Current;
			}


			try
			{
				//EASendMail.SmtpMail oMail = new EASendMail.SmtpMail("TryIt");
				//oMail.From = "desarrollo@calisis.com";
				//oMail.To = "transferoee@gmail.com";
				//oMail.Subject = "#SMS# Sampras OEE";
				//oMail.TextBody = Numbers + textoRectificado;
				//SmtpServer oServer = new SmtpServer("smtp.gmail.com");
				//oServer.User = "desarrollo@calisis.com";
				//oServer.Password = "march2018";
				//oServer.Port = 587;
				//oServer.ConnectType = SmtpConnectType.ConnectSSLAuto;
				//EASendMail.SmtpClient oSmtp = new EASendMail.SmtpClient();
				//oSmtp.SendMail(oServer, oMail);

				using (System.Net.Mail.MailMessage mm = new System.Net.Mail.MailMessage("desarrollo@calisis.com", "transferoee@gmail.com"))
				{
					mm.From = new System.Net.Mail.MailAddress("desarrollo@calisis.com", "Sampras");
					mm.Subject = "#SMS# Sampras OEE";
					mm.Body = Numbers + textoRectificado;

					mm.IsBodyHtml = false;
					Mail_Configuration MailConfigurationObject = new Mail_Configuration();
					using (System.IO.StreamReader file = System.IO.File.OpenText(System.Web.HttpContext.Current.Server.MapPath("~/mailConfig.json")))
					{
						MailConfigurationObject = new Mail_Configuration(file);
					}

					using (System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient())
					{
						smtp.Host = MailConfigurationObject.SmtpServer;
						smtp.EnableSsl = true;
						NetworkCredential NetworkCred = new NetworkCredential(MailConfigurationObject.mailUser, MailConfigurationObject.mailPass);
						smtp.UseDefaultCredentials = true;
						smtp.Credentials = NetworkCred;
						smtp.Port = MailConfigurationObject.port;
						smtp.Send(mm);

					}
				}




				return true;
			}
			catch (Exception e)
			{
				return false;
			}
		}

		[Route("api/SMS/sender")]
		public bool Getsender(string Numbers, string Texto, string Languaje)
		{
			if (Numbers == null)
			{
				Numbers = "";
			}

			string textoRectificado = "";

			foreach (var item in Texto)
			{
				char Current = item;
				if (Current == 'á')
				{
					Current = 'a';
				}
				if (Current == 'é')
				{
					Current = 'e';
				}
				if (Current == 'í')
				{
					Current = 'i';
				}
				if (Current == 'ó')
				{
					Current = 'o';
				}
				if (Current == 'ú')
				{
					Current = 'u';
				}
				textoRectificado += Current;
			}


			try
			{
				//EASendMail.SmtpMail oMail = new EASendMail.SmtpMail("TryIt");
				//oMail.From = "desarrollo@calisis.com";
				//oMail.To = "transferoee@gmail.com";
				//oMail.Subject = "#SMS# Sampras OEE";
				//oMail.TextBody = Numbers + textoRectificado + "|" + Languaje; ;
				//SmtpServer oServer = new SmtpServer("smtp.gmail.com");
				//oServer.User = "desarrollo@calisis.com";
				//oServer.Password = "march2018";
				//oServer.Port = 587;
				//oServer.ConnectType = SmtpConnectType.ConnectSSLAuto;
				//EASendMail.SmtpClient oSmtp = new EASendMail.SmtpClient();
				//oSmtp.SendMail(oServer, oMail);

				using (System.Net.Mail.MailMessage mm = new System.Net.Mail.MailMessage("desarrollo@calisis.com", "transferoee@gmail.com"))
				{
					mm.From = new System.Net.Mail.MailAddress("desarrollo@calisis.com", "Sampras");
					mm.Subject = "#SMS# Sampras OEE";
					mm.Body = Numbers + textoRectificado + Languaje; ;
					Mail_Configuration MailConfigurationObject = new Mail_Configuration();
					using (System.IO.StreamReader file = System.IO.File.OpenText(System.Web.HttpContext.Current.Server.MapPath("~/mailConfig.json")))
					{
						MailConfigurationObject = new Mail_Configuration(file);
					}
					mm.IsBodyHtml = false;
					using (System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient())
					{
						smtp.Host = MailConfigurationObject.SmtpServer;
						smtp.EnableSsl = true;
						NetworkCredential NetworkCred = new NetworkCredential(MailConfigurationObject.mailUser, MailConfigurationObject.mailPass);
						smtp.UseDefaultCredentials = true;
						smtp.Credentials = NetworkCred;
						smtp.Port = MailConfigurationObject.port;
						smtp.Send(mm);
					}

				}


				return true;
			}
			catch (Exception e)
			{
				return false;
			}
		}

		[Route("api/Email/mailStop")]
		public bool GetmailStop(string mail, string labelColumn, string valueColumn)
		{
			string[] labelArray = labelColumn.Split('|');
			string[] valueArray = valueColumn.Split('|');

			string HTPMstr = "<html><title>SamprasOEE Inicio de paro por mtto. LA012</title><body><div align=left><table style='font-family: Verdana; font-size: 7.5pt; padding:1px; width:700px'><tr valing=top style='background-color: #848484; color:White; height:22px;'><td colspan=2>&nbsp;<b>&nbsp;Notificación automática de paro a través del sistema SamprasOEE</b></td></tr><tr valing=top style='background-color:White; color:Black; height:22px;'><td colspan=2>&nbsp;<b>&nbsp;Información de Paro</b></td></tr><tr valing=top style='background-color: #CEE3F6; height:22px;'><td style='width:200px'>&nbsp;<b>" + labelArray[0] + ":</b></td><td style='width:600px'>&nbsp;" + valueArray[0] + "</td></tr><tr valing=top style='background-color: #CEE3F6; height:22px;'><td style='width:200px'>&nbsp;<b>" + labelArray[1] + ":</b></td><td style='width:600px'>&nbsp;" + valueArray[1] + "</td></tr><tr valing=top style='background-color: #CEE3F6; height:22px;'><td style='width:200px'>&nbsp;<b>" + labelArray[2] + ":</b></td><td style='width:600px'>&nbsp;" + valueArray[2] + "</td></tr><tr valing=top style='background-color: #CEE3F6; height:22px;'><td style='width:200px'>&nbsp;<b>" + labelArray[3] + ":</b></td><td style='width:600px'>&nbsp;" + valueArray[3] + "</td></tr><tr valing=top style='background-color: #CEE3F6; height:22px;'><td style='width:200px'>&nbsp;<b>" + labelArray[4] + ":</b></td><td style='width:600px'>&nbsp;" + valueArray[4] + "</td></tr><tr valing=top style='background-color: #CEE3F6; height:22px;'><td style='width:200px'>&nbsp;<b>" + labelArray[5] + ":</b></td><td style='width:600px'>&nbsp;" + valueArray[5] + "</td></tr><tr valing=top style='background-color:White; color:Black; height:22px;'><td colspan=2>&nbsp;<b>&nbsp;Información de Producción</b></td></tr><tr valing=top style='background-color: #CEE3F6; height:22px;'><td style='width:200px'>&nbsp;<b>" + labelArray[6] + ":</b></td><td style='width:600px'>&nbsp;" + valueArray[6] + "</td></tr><tr valing=top style='background-color: #CEE3F6; height:22px;'><td style='width:200px'>&nbsp;<b>" + labelArray[7] + ":</b></td><td style='width:600px'>&nbsp;" + valueArray[7] + "</td></tr><tr valing=top style='background-color: #CEE3F6; height:22px;'><td style='width:200px'>&nbsp;<b>" + labelArray[8] + ":</b></td><td style='width:600px'>&nbsp;" + valueArray[8] + "</td></tr><tr valing=top style='background-color: #CEE3F6; height:22px;'><td style='width:200px'>&nbsp;<b>" + labelArray[9] + ":</b></td><td style='width:600px'>&nbsp;" + valueArray[9] + "</td></tr><tr valing=top style='background-color: #CEE3F6; height:22px;'><td style='width:200px'>&nbsp;<b>" + labelArray[10] + ":</b></td><td style='width:600px'>&nbsp;" + valueArray[10] + "</td></tr><tr valing=top style='background-color: #CEE3F6; height:22px;'><td style='width:200px'>&nbsp;<b>" + labelArray[11] + ":</b></td><td style='width:600px'>&nbsp;" + valueArray[11] + "</td></tr><tr valing=top style='background-color: #CEE3F6; height:22px;'><td style='width:200px'>&nbsp;<b>" + labelArray[12] + ":</b></td><td style='width:600px'>&nbsp;" + valueArray[12] + "</td></tr><tr style='height:22px;'><td colspan=2>&nbsp;SamprasOEE - Calisis&nbsp;&nbsp;" + valueArray[13] + "</td></tr></table></div></body></html>";

			try
			{
				//EASendMail.SmtpMail oMail = new EASendMail.SmtpMail("TryIt");
				//oMail.From = new EASendMail.MailAddress("desarrollo@calisis.com", "Sampras");
				////oMail.Headers.Add("Sender", "displayName@aliasDomain.com");
				//oMail.To = mail;
				//oMail.Subject = "Notificación automática de paro a través del sistema SamprasOEE";
				//oMail.ReplyTo = "displayName@aliasDomain.com";
				////oMail.TextBody = HTPMstr;
				//oMail.HtmlBody = HTPMstr;
				//SmtpServer oServer = new SmtpServer("smtp.gmail.com");
				//oServer.User = "desarrollo@calisis.com";
				//oServer.Password = "march2018";
				//oServer.Port = 587;
				//oServer.ConnectType = SmtpConnectType.ConnectSSLAuto;
				//EASendMail.SmtpClient oSmtp = new EASendMail.SmtpClient();
				//oSmtp.SendMail(oServer, oMail);


				using (System.Net.Mail.MailMessage mm = new System.Net.Mail.MailMessage("desarrollo@calisis.com", mail))
				{
					mm.From = new System.Net.Mail.MailAddress("desarrollo@calisis.com", "Sampras");
					mm.Subject = "SamprasOEE Notification";
					mm.Body = HTPMstr;

					mm.IsBodyHtml = true;

					Mail_Configuration MailConfigurationObject = new Mail_Configuration();
					using (System.IO.StreamReader file = System.IO.File.OpenText(System.Web.HttpContext.Current.Server.MapPath("~/mailConfig.json")))
					{
						MailConfigurationObject = new Mail_Configuration(file);
					}

					using (System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient())
					{
						smtp.Host = MailConfigurationObject.SmtpServer;
						smtp.EnableSsl = true;
						NetworkCredential NetworkCred = new NetworkCredential(MailConfigurationObject.mailUser, MailConfigurationObject.mailPass);
						smtp.UseDefaultCredentials = true;
						smtp.Credentials = NetworkCred;
						smtp.Port = MailConfigurationObject.port;
						smtp.Send(mm);
					}
				}






				return true;
			}
			catch (Exception e)
			{
				return false;
			}
		}

		[Route("api/Email/sendattachedmail")]
		public bool Getsendattachedmail(string mail, string filename, string body)
		{

			List<string> AdressList = mail.Split(',').ToList();
			String Route = System.Configuration.ConfigurationManager.AppSettings["Route"];
			try
			{
				foreach (var item in AdressList)
				{
					using (System.Net.Mail.MailMessage mm = new System.Net.Mail.MailMessage("desarrollo@calisis.com", item))
					{
						mm.From = new System.Net.Mail.MailAddress("desarrollo@calisis.com", "Sampras");
						mm.Subject = "SamprasOEE Notification";
						mm.IsBodyHtml = true;
						mm.Body = body;
						string CompletePath = Route + filename;
						System.Net.Mail.Attachment data = new System.Net.Mail.Attachment(CompletePath, MediaTypeNames.Application.Octet);
						mm.Attachments.Add(data);
						mm.IsBodyHtml = false;
						Mail_Configuration MailConfigurationObject = new Mail_Configuration();
						using (System.IO.StreamReader file = System.IO.File.OpenText(System.Web.HttpContext.Current.Server.MapPath("~/mailConfig.json")))
						{
							MailConfigurationObject = new Mail_Configuration(file);
						}

						using (System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient())
						{
							smtp.Host = MailConfigurationObject.SmtpServer;
							smtp.EnableSsl = true;

							NetworkCredential NetworkCred = new NetworkCredential(MailConfigurationObject.mailUser, MailConfigurationObject.mailPass);
							smtp.UseDefaultCredentials = true;
							smtp.Credentials = NetworkCred;
							smtp.Port = MailConfigurationObject.port;
							smtp.Send(mm);
						}
					}

				}

				return true;
			}
			catch (Exception e)
			{
				return false;
			}
		}

		[Route("api/Email/mailstop_list")]
		public bool GetmailStop_list(string mailList, string labelColumn, string valueColumn)
		{
			mailList = mailList.Replace(';', ',');
			List<string> AdressList = mailList.Split(',').ToList();

			string[] labelArray = labelColumn.Split('|');
			string[] valueArray = valueColumn.Split('|');

			string HTPMstr = "<html><title>SamprasOEE Inicio de paro por mtto. LA012</title><body><div align=left><table style='font-family: Verdana; font-size: 7.5pt; padding:1px; width:700px'><tr valing=top style='background-color: #848484; color:White; height:22px;'><td colspan=2>&nbsp;<b>&nbsp;Notificación automática de paro a través del sistema SamprasOEE</b></td></tr><tr valing=top style='background-color:White; color:Black; height:22px;'><td colspan=2>&nbsp;<b>&nbsp;Información de Paro</b></td></tr><tr valing=top style='background-color: #CEE3F6; height:22px;'><td style='width:200px'>&nbsp;<b>" + labelArray[0] + ":</b></td><td style='width:600px'>&nbsp;" + valueArray[0] + "</td></tr><tr valing=top style='background-color: #CEE3F6; height:22px;'><td style='width:200px'>&nbsp;<b>" + labelArray[1] + ":</b></td><td style='width:600px'>&nbsp;" + valueArray[1] + "</td></tr><tr valing=top style='background-color: #CEE3F6; height:22px;'><td style='width:200px'>&nbsp;<b>" + labelArray[2] + ":</b></td><td style='width:600px'>&nbsp;" + valueArray[2] + "</td></tr><tr valing=top style='background-color: #CEE3F6; height:22px;'><td style='width:200px'>&nbsp;<b>" + labelArray[3] + ":</b></td><td style='width:600px'>&nbsp;" + valueArray[3] + "</td></tr><tr valing=top style='background-color: #CEE3F6; height:22px;'><td style='width:200px'>&nbsp;<b>" + labelArray[4] + ":</b></td><td style='width:600px'>&nbsp;" + valueArray[4] + "</td></tr><tr valing=top style='background-color: #CEE3F6; height:22px;'><td style='width:200px'>&nbsp;<b>" + labelArray[5] + ":</b></td><td style='width:600px'>&nbsp;" + valueArray[5] + "</td></tr><tr valing=top style='background-color:White; color:Black; height:22px;'><td colspan=2>&nbsp;<b>&nbsp;Información de Producción</b></td></tr><tr valing=top style='background-color: #CEE3F6; height:22px;'><td style='width:200px'>&nbsp;<b>" + labelArray[6] + ":</b></td><td style='width:600px'>&nbsp;" + valueArray[6] + "</td></tr><tr valing=top style='background-color: #CEE3F6; height:22px;'><td style='width:200px'>&nbsp;<b>" + labelArray[7] + ":</b></td><td style='width:600px'>&nbsp;" + valueArray[7] + "</td></tr><tr valing=top style='background-color: #CEE3F6; height:22px;'><td style='width:200px'>&nbsp;<b>" + labelArray[8] + ":</b></td><td style='width:600px'>&nbsp;" + valueArray[8] + "</td></tr><tr valing=top style='background-color: #CEE3F6; height:22px;'><td style='width:200px'>&nbsp;<b>" + labelArray[9] + ":</b></td><td style='width:600px'>&nbsp;" + valueArray[9] + "</td></tr><tr valing=top style='background-color: #CEE3F6; height:22px;'><td style='width:200px'>&nbsp;<b>" + labelArray[10] + ":</b></td><td style='width:600px'>&nbsp;" + valueArray[10] + "</td></tr><tr valing=top style='background-color: #CEE3F6; height:22px;'><td style='width:200px'>&nbsp;<b>" + labelArray[11] + ":</b></td><td style='width:600px'>&nbsp;" + valueArray[11] + "</td></tr><tr valing=top style='background-color: #CEE3F6; height:22px;'><td style='width:200px'>&nbsp;<b>" + labelArray[12] + ":</b></td><td style='width:600px'>&nbsp;" + valueArray[12] + "</td></tr><tr style='height:22px;'><td colspan=2>&nbsp;SamprasOEE - Calisis&nbsp;&nbsp;" + valueArray[13] + "</td></tr></table></div></body></html>";


			try
			{
				foreach (var item in AdressList)
				{


					try
					{
						//SmtpMail oMail = new SmtpMail("TryIt");

						//oMail.From = "desarrollo@calisis.com";
						//oMail.To = item;
						//oMail.Subject = "Notificación automática de paro a través del sistema SamprasOEE";
						//oMail.HtmlBody = HTPMstr;
						//SmtpServer oServer = new SmtpServer("smtp.gmail.com");
						//oServer.User = "desarrollo@calisis.com";
						//oServer.Password = "march2018";
						//oServer.Port = 587;
						//oServer.ConnectType = SmtpConnectType.ConnectSSLAuto;
						//SmtpClient oSmtp = new SmtpClient();
						//oSmtp.SendMail(oServer, oMail);


						using (System.Net.Mail.MailMessage mm = new System.Net.Mail.MailMessage("desarrollo@calisis.com", item))
						{
							mm.From = new System.Net.Mail.MailAddress("desarrollo@calisis.com", "Sampras");
							mm.Subject = "SamprasOEE Notification";
							mm.Body = HTPMstr;
							mm.IsBodyHtml = true;
							Mail_Configuration MailConfigurationObject = new Mail_Configuration();
							using (System.IO.StreamReader file = System.IO.File.OpenText(System.Web.HttpContext.Current.Server.MapPath("~/mailConfig.json")))
							{
								MailConfigurationObject = new Mail_Configuration(file);
							}

							using (System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient())
							{
								smtp.Host = MailConfigurationObject.SmtpServer;
								smtp.EnableSsl = true;
								NetworkCredential NetworkCred = new NetworkCredential(MailConfigurationObject.mailUser, MailConfigurationObject.mailPass);
								smtp.UseDefaultCredentials = true;
								smtp.Credentials = NetworkCred;
								smtp.Port = MailConfigurationObject.port;
								smtp.Send(mm);
							}
						}



					}
					catch
					{

					}
				}
				return true;
			}
			catch
			{
				return false;
			}
		}

		[Route("api/Email/SendMailTemplate")]
		public string GetSendMailTemplate(string Map_Values, string AddressesList, string id_Template)
		{
			int Num_Id_Template = int.Parse(id_Template);
			Tbl_MailTemplateTable ObjTemplateData = db.Tbl_MailTemplateTable.Find(Num_Id_Template);

			string HTMLTemplate_Body = ObjTemplateData.Chart_Template_Body;

			List<string> List_Component = Map_Values.Split(new string[] { "||" }, StringSplitOptions.None).ToList();

			foreach (var item in List_Component)
			{
				List<string> Current_Value = item.Split(new string[] { "**" }, StringSplitOptions.None).ToList();
				HTMLTemplate_Body = HTMLTemplate_Body.Replace(Current_Value.ElementAt(0), Current_Value.ElementAt(1));
			}

			List<string> List_Address = AddressesList.Split(new string[] { "," }, StringSplitOptions.None).ToList();

			foreach (var item in List_Address)
			{
				if (IsEmailValid(item) == true)
				{
					try
					{
						using (System.Net.Mail.MailMessage mm = new System.Net.Mail.MailMessage("desarrollo@calisis.com", item))
						{
							mm.From = new System.Net.Mail.MailAddress("desarrollo@calisis.com", "Sampras");
							mm.Subject = "SamprasOEE Notification";
							mm.Body = HTMLTemplate_Body;

							mm.IsBodyHtml = true;
							Mail_Configuration MailConfigurationObject = new Mail_Configuration();

							using (System.IO.StreamReader file = System.IO.File.OpenText(System.Web.HttpContext.Current.Server.MapPath("~/mailConfig.json")))
							{
								MailConfigurationObject = new Mail_Configuration(file);
							}

							using (System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient())
							{
								smtp.Host = MailConfigurationObject.SmtpServer;
								smtp.EnableSsl = true;

								NetworkCredential NetworkCred = new NetworkCredential(MailConfigurationObject.mailUser, MailConfigurationObject.mailPass);
								smtp.UseDefaultCredentials = true;
								smtp.Credentials = NetworkCred;
								smtp.Port = MailConfigurationObject.port;
								smtp.Send(mm);
							}
						}
						
					}
					catch (Exception e)
					{
						return e.Message.ToString();
					}
				}
			}

			return "True";
		}

		[Route("api/Email/SendMailNotification")]
		public string GetSendMailNotification(string AddressesList, string TextNotification)
		{
			List<string> List_Address = AddressesList.Split(new string[] { "||" }, StringSplitOptions.None).ToList();

			foreach (var item in List_Address)
			{
				try
				{
					//using (System.Net.Mail.MailMessage mm = new System.Net.Mail.MailMessage("desarrollo@calisis.com", item))
					//{
					//	mm.From = new System.Net.Mail.MailAddress("desarrollo@calisis.com", "Sampras");
					//	mm.Subject = "SamprasOEE Notification";
					//	mm.Body = TextNotification;

					//	mm.IsBodyHtml = false;
						Mail_Configuration MailConfigurationObject = new Mail_Configuration();

					using (System.IO.StreamReader file = System.IO.File.OpenText(System.Web.HttpContext.Current.Server.MapPath("~/mailConfig.json")))
					{
						MailConfigurationObject = new Mail_Configuration(file);
					}

					//	using (System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient())
					//	{
					//		smtp.Host = MailConfigurationObject.SmtpServer;
					//		smtp.EnableSsl = true;
					//		smtp.UseDefaultCredentials = false;
					//		NetworkCredential NetworkCred = new NetworkCredential(MailConfigurationObject.mailUser, MailConfigurationObject.mailPass);

					//		smtp.Credentials = NetworkCred;
					//		smtp.Port = MailConfigurationObject.port;
					//		smtp.Send(mm);
					//	}
					//}


					System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient();
					client.Host = "smtp.gmail.com";
					client.Port = 587;
					client.EnableSsl = true;
					client.UseDefaultCredentials = false;
					client.Credentials = new NetworkCredential(MailConfigurationObject.mailUser, MailConfigurationObject.mailPass);
					
					client.DeliveryMethod = SmtpDeliveryMethod.Network;

					System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
					mail.From = new System.Net.Mail.MailAddress("transferoee@gmail.com", "Sender");
					mail.To.Add(new System.Net.Mail.MailAddress(item));
					mail.Subject = "TEST";
					mail.IsBodyHtml = true;
					mail.Body = TextNotification;

					client.Send(mail);


				}
				catch (Exception e)
				{
					return  e.Message;
				}
			}

			return "true";
		}

		public bool IsEmailValid(string emailaddress)
		{
			try
			{
				EASendMail.MailAddress m = new EASendMail.MailAddress(emailaddress);
				return true;
			}
			catch (FormatException)
			{
				return false;
			}
		}

		[Route("api/SMS/SendBulletin")]
		public bool GetSendBulletin(string AddressesList, string Values_To_Chart)
		{
			try
			{


				return true;
			}
			catch
			{
				return false;
			}
		}

		// POST api/values
		public void Post([FromBody]string value)
		{
		}

		// PUT api/values/5
		public void Put(int id, [FromBody]string value)
		{
		}

		// DELETE api/values/5
		public void Delete(int id)
		{
		}
	}
}
