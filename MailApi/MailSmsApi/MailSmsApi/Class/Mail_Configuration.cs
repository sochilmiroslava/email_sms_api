﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace MailSmsApi.Class
{
	public class Mail_Configuration
	{
		public string SmtpServer;
		public string mailUser;
		public string mailPass;
		public int port;

		public Mail_Configuration()
		{
		}


		public Mail_Configuration(StreamReader file)
		{
			JObject o2 = null;
			using (JsonTextReader reader = new JsonTextReader(file))
			{
				o2 = (JObject)JToken.ReadFrom(reader);
			}

			this.SmtpServer = o2.GetValue("SmtpServer").ToString();
			this.mailUser = o2.GetValue("mailUser").ToString();
			this.mailPass = o2.GetValue("mailPass").ToString();
			this.port = int.Parse(o2.GetValue("port").ToString());

		}
	}

}